package com.inin.ticket_app.ui.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.inin.ticket_app.R;
import com.inin.ticket_app.views.IMainView;

public class MainActivity extends AppCompatActivity implements IMainView{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
